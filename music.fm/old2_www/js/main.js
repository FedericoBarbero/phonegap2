
// The Audio player
var my_media = null;
var mediaTimer = null;
// duration of media (song)
var dur = -1;
// need to know when paused or not
var is_paused = false;
var src = '';
var t = null; //timer

function init() {
    
    document.addEventListener("deviceready", onDR, false);
}
//Set audio position on page
function setAudioPosition(position) {
    $("#audio_position").html(position + " sec");
}

//onSuccess Callback
function onSuccess() {
    setAudioPosition(dur);
    clearInterval(mediaTimer);
    mediaTimer = null;
    my_media = null;
    is_paused = false;
    dur = -1;
}

//onError Callback 
function onError(error) {
    alert('code: '    + error.code    + '\n' + 
            'message: ' + error.message + '\n');
    clearInterval(mediaTimer);
    mediaTimer = null;
    my_media = null;
    is_paused = false;
    setAudioPosition("0");
}

function playAudio(src) {
    console.log("Play Audio " + src);
    if (my_media) {
	my_media.stop();
    my_media.release();
    my_media = null;
    } 
    my_media = new Media(src,onSuccess,onError);
    // Create Media object from src, and set the onSuccess and onError methods
    my_media.play();
     // Play audio src
    dur = my_media.getDuration();
    // Update my_media position every second

    t = setInterval(function(){
        pos = my_media.getCurrentPosition();
        setAudioPosition(pos);
    },1000);

   // get duration

   if (dur > 0) //, update the interface using 
        docuement.getElementById('media_dur').innerHTML=(dur) + "sec";	
   //OR 
   
}

//Pause audio
function pauseAudio() {
     if (is_paused) {
        if (my_media) {
            is_paused = false;
            my_media.play();
            $("#pauseaudio").text("Pause");
        }
    } else {
        if (my_media) {
            is_paused = true;
            my_media.pause();
            $("#pauseaudio").text("Play");
        }
    }
}

//Stop audio
function stopAudio() {
    if (my_media) {
        // A successful .stop() will call .release()
        my_media.stop();
        my_media.release();
        my_media = null;
    }
    if (mediaTimer) {
        clearInterval(mediaTimer);
        mediaTimer = null;
    }
    is_paused = false;
    dur = 0;
    clearInterval(t);
    t = null;
}

/* Concert  */
 
function searchConcerts(nameArtist, nameCity) {
//	var url = http://api.setlist.fm/rest/0.1/search/setlist.json?artistName="nameArtist"&cityName="nameCity";
	var serviceURL = "http://api.setlist.fm/rest/0.1/search/setlists.json?"; 
	var parameter = new String();
	parameter = parameter.concat("artistName=", nameArtist, "&cityName=", nameCity);
	var url = serviceURL + "" + parameter;
	console.log(url);
   
     $.ajax({
        url : serviceURL + "" + parameter,
        type : "GET",
	 dataType : "json",
        success : parseConcert,
        error : showError
});

}

function parseConcert(dataJson){
    console.log("response " + dataJson);

}

/* Tour */ 


function searchTour(nameArtist,nameTour){
    var serviceURL = "http://api.setlist.fm/rest/0.1/search/setlists.json?"; 
    var parameter = new String();
    // SEE searchConcert

    $.ajax({
	    url : serviceURL + "" + parameter,
		type : "GET",
		//        dataType : "json",
		success : parseTour,
		error : showError
		});

}

function parseTour(dataJson){
    console.log("response " + dataJson);
    
    
}

/**
 * Callback ajax error
 * @param request
 * @param error
 * @param obj
 */
function showError(request, error, obj) {
        console.log("Error received" + error + " " + request);
        alert("Error contacting remote Server.");
}

$( function() {
		
	/*$('#playaudio').click(function() {
        // Note: two ways to access media file: web and local file        
        //src = 'http://audio.ibeat.org/content/p1rj1s/p1rj1s_-_rockGuitar.mp3';
        
        // local (on device): copy file to project's /assets folder:
        //var src = '/android_asset/spittinggames.m4a';
        
        playAudio(src);
    });*/

    // Start with Manual selected and Flip Mode hidden
    $('#nav-manual').focus();
    $('#content-list').hide();
   
    $('#nav-manual').live('tap', function() {
    	console.log("nav-manual taped");
        $('#content-list').hide();
        $('#content-manual').show();
        stopAudio();
    });
   
    $('#nav-list').live('tap', function() {
        $('#content-manual').hide();
        $('#content-list').show();
    });
	

    $("#pauseaudio").live('tap', function() {
        pauseAudio();
    });
    
    $("#stopaudio").live('tap', function() {
        stopAudio();
    });
    
    $('#music1').click(function() {
    	src = 'http://www.universal-soundbank.com/mp3/sounds/10157.mp3';
    	playAudio(src);
    });
    
    $('#music2').click(function() {
    	src = 'http://www.universal-soundbank.com/mp3/sounds/10183.mp3';
    	playAudio(src);
    });
    
    $('#music3').click(function() {
    	src = 'http://www.universal-soundbank.com/mp3/sounds/10153.mp3';
    	playAudio(src);
    });
    
    $('#music4').click(function() {
    	src = 'http://stream1.addictradio.net/addictrock.mp3';
    	playAudio(src);
    });
    
    $("#searchButtonConcert").click(function() {
	    // get the vars : artistName and cityName
	    artistName=$('#artistNameConcert').val();	
	    nameCity=$('#cityName').val();
	    // call the function to make an ajax request
        searchConcerts(artistName, nameCity);
    });
    
    $("#searchButtonTour").click(function() {

	    // searchTour(nameArtist, nameTour);
    });
    
    // implement a function to read a file from the sdcard
    
});


