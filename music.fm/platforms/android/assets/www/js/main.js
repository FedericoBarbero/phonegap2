
// The Audio player
var my_media = null;
var mediaTimer = null;
// duration of media (song)
var dur = -1;
// need to know when paused or not
var is_paused = false;
var src = '';
//var t = null; //timer

//function init() {
 
//    document.addEventListener("deviceready", onDR, false);
//}
//Set audio position on page
function setAudioPosition(position) {
    $("#audio_position").html(position + " sec");
}

//onSuccess Callback
var onSuccess = function() {
    setAudioPosition(dur);
    clearInterval(mediaTimer);
    mediaTimer = null;
    if(my_media)
        my_media.release();
    my_media = null;
    is_paused = false;
    dur = -1;
}

//onError Callback 
function onError(error) {
    alert('code: '    + error.code    + '\n' + 
            'message: ' + error.message + '\n');
    clearInterval(mediaTimer);
    mediaTimer = null;
    my_media.release();
    my_media = null;
    is_paused = false;
    setAudioPosition("0");
}

function playAudio(src) {
    console.log("Play Audio " + src);
    if (my_media) {
    my_media.stop();
    my_media.release();
    my_media = null;
    } 
    my_media = new Media(src, onSuccess, onError);
    console.log("Media " + my_media);
    // Create Media object from src, and set the onSuccess and onError methods
    my_media.play();
    // Play audio src
    dur = my_media.getDuration();
    //Update my_media position every second

    mediaTimer = setInterval(function(){
        if(my_media != null)
            my_media.getCurrentPosition(setAudioPosition);
    },1000);

   //get duration

   if (dur > 0) //, update the interface using 
       document.getElementById('media_dur').innerHTML=(dur) + "sec";  
   //OR 
   
}

//Pause audio
function pauseAudio() {
    console.log('pressed pause audio');
     if (is_paused) {
        if (my_media) {
            is_paused = false;
            my_media.play();
            $("#pauseaudio").text("Pause");
        }
    } else {
        if (my_media) {
            is_paused = true;
            my_media.pause();
            $("#pauseaudio").text("Play");
        }
    }
}

//Stop audio
function stopAudio() {
    if (my_media) {
        // A successful .stop() will call .release()
        my_media.stop();
        my_media.release();
        my_media = null;
    }
    if (mediaTimer) {
        clearInterval(mediaTimer);
        mediaTimer = null;
    }
    is_paused = false;
    dur = 0;
}

/* Concert  */

function formatConcert(setlist){
    var date = setlist["@eventDate"];
    var tour = setlist["@tour"];
    var venue = (setlist.venue)["@name"];
    var city = (setlist.venue.city)["@name"];
    var venueUrl = setlist.venue.url;
    var url = setlist.url;

    dateEl = $('<b/>').html(date);
    cityEl = $('<b/>').html(city);
    tourEl = $('<b/>').html(tour);
    venueEl = $('<a/>').attr('href', venueUrl).html(venue);
    newlineEl = $('<br/>');
    el = $('<p/>').css('border-bottom-width', '1px').append(dateEl).append(newlineEl).append(tourEl).append(newlineEl).append(venueEl);
    return el;
}
 
function searchConcerts(nameArtist, nameCity) {
    $("#resultTours").html("");
//  var url = http://api.setlist.fm/rest/0.1/search/setlist.json?artistName="nameArtist"&cityName="nameCity";
    var serviceURL = "http://api.setlist.fm/rest/0.1/search/setlists.json?"; 
    var parameter = new String();
    parameter = parameter.concat("artistName=", nameArtist, "&cityName=", nameCity);
    var url = serviceURL + "" + parameter;
    console.log(url);
   
     $.ajax({
        url : serviceURL + "" + parameter,
        type : "GET",
     dataType : "json",
        success : parseConcert,
        error : showError
});

}

function parseConcert(dataJson){
    console.log("response " + dataJson);
    
    var setlists = dataJson.setlists.setlist;
    
    var theHtml = new String("<h4>concerts</h4>");    
    
    for (i=0; i<setlists.length; i++){
        var date = (setlists[i])["@eventDate"];
        var tour = (setlists[i])["@tour"];
        var venue = (setlists[i].venue)["@name"];
        var city = (setlists[i].venue.city)["@name"];
        var venueUrl = setlists[i].venue.url;
        var url = setlists[i].url;
        
        theHtml = theHtml.concat("<p style=\"border-bottom-width:1px; border-bottom-style:solid; padding-bottom-4px;\"><b>Tour:</b> ",tour,
                                "<br><b>Date:</b> ", date,
                                "<br><b>City:</b> ", city,
                                 "<br><b><a href=\"",venueUrl,"\">",venue,"</a></b>",
                                 "<br><b>link</b>: <a href=\"",url,"\">",url,"</a></p>");
    }
    
    $("#resultConcert").html(theHtml);
    
}

/* Tour */ 


function searchTour(nameArtist,nameTour){
    $("#resultTours").html("");
    var serviceURL = "http://api.setlist.fm/rest/0.1/search/setlists.json?"; 
    var parameter = new String();
    parameter = parameter.concat("artistName=", nameArtist, "&tour=", nameTour);
    var url = serviceURL + "" + parameter;
    url = encodeURI(url);
    console.log(url);

    $.ajax({
        url : serviceURL + "" + parameter,
        type : "GET",
        dataType : "json",
        success : parseTour,
        error : showError
        });

}

function parseTour(dataJson){
    console.log("response " + dataJson);
    
    var setlists = dataJson.setlists.setlist;
    
    var theHtml = new String("<h4>concerts</h4>");    
    
    for (i=0; i<setlists.length; i++){
        var date = (setlists[i])["@eventDate"];
        var tour = (setlists[i])["@tour"];
        var venue = (setlists[i].venue)["@name"];
        var venueUrl = setlists[i].venue.url;
        var city = (setlists[i].venue.city)["@name"];
        var url = setlists[i].url;
        
        theHtml = theHtml.concat("<p style=\"border-bottom-width:1px; border-bottom-style:solid; padding-bottom-4px;\"><b>Tour:</b> ",tour,
                                "<br><b>Date:</b> ", date,
                                "<br><b>City:</b> ", city,
                                 "<br><b><a href=\"",venueUrl,"\">",venue,"</a></b>",
                                 "<br><b>link</b>: <a href=\"",url,"\">",url,"</a></p>");
    }
    
    $("#resultTours").html(theHtml);
}

/**
 * Callback ajax error
 * @param request
 * @param error
 * @param obj
 */
function showError(request, error, obj) {
        console.log("Error received" + error + " " + request);
        alert("No data available!");
}

// <!-- SDCard -->
function fileButton(file) {
    return $("<p>").attr({'data-role': 'button', 'uri': file.toURL()}).addClass('music-file').append($("<small>").html(file.name));
}
function directoryButton(file) {
    return $("<p>").attr({'data-role': 'button', 'data-icon': 'arrow-r', 'data-iconpos': 'right', 'uri': file.toURL().substring(7)}).addClass('directory').append($("<small>").html(file.name));
}
function parentButton(path) {
    return $("<p>").attr({'data-role': 'button', 'data-icon': 'back', 'uri': path}).addClass('directory').append($("<small>").html('..'));
}

function onReadEntries(entries) {
    var i;
    $musicFiles = $sdgrid.find("#music-files");
    $directories = $sdgrid.find("#directories");
    if(parentDirectoryPath != null) {
        $sdgrid.find("#parent-directory").append(parentButton(parentDirectoryPath));
    }
    
    var ext = new RegExp('\\.(mp3|wav|m4a)$'); //check ext
    for (i = 0; i < entries.length; i++) {
        if (entries[i].isFile == true) {
            if(ext.test(entries[i].name)) {
                $musicFiles.append(fileButton(entries[i]));
            }
        }
        else if (entries[i].isDirectory == true) {
            $directories.append(directoryButton(entries[i]));
        }
    }
    $sdgrid.find("#parent-directory").children().button();
    $musicFiles.children().sort(uriCompare).appendTo($musicFiles).button();
    $directories.children().sort(uriCompare).appendTo($directories).button();
}

function uriCompare(a,b) {
    aa=a.getAttribute('uri');
    ba=b.getAttribute('uri');
    return aa>ba ? 1 : (aa<ba ? -1 : 0);
}
function parseEntries(dirReader){
    $sdgrid.html("");
    $sdgrid.append($('<span>').attr('id', 'parent-directory'));
    $sdgrid.append($('<span>').attr('id', 'music-files'));
    $sdgrid.append($('<span>').attr('id', 'directories'));
    $("#directory-path").empty().append($("<h5>").html(directoryPath));
    if(dirReader){
        dirReader.readEntries(onReadEntries, fail);
    }
}

$( function() {
		
	$('#playaudio').click(function() {
        // Note: two ways to access media file: web and local file        
        src = 'http://audio.ibeat.org/content/p1rj1s/p1rj1s_-_rockGuitar.mp3';
        
        // local (on device): copy file to project's /assets folder:
        //var src = '/android_asset/spittinggames.m4a';
        
        playAudio(src);
    });

    // Start with Manual selected and Flip Mode hidden
    $('#nav-manual').focus();
    $('#content-list1').hide();
    $('#content-list2').hide();
    $('#content-list3').hide();
    $('#content-list4').hide();
   
    $('#nav-manual').live('tap', function() {
    	console.log("nav-manual taped");
        $('#content-list1').hide();
	$('#content-list2').hide();
	$('#content-list3').hide();
	$('#content-list4').hide();
        $('#content-manual').show();
        stopAudio();
    });
   
    $('#nav-list1').live('tap', function() {
        $('#content-manual').hide();	
	$('#content-list2').hide();
	$('#content-list3').hide();
	$('#content-list4').hide();
        $('#content-list1').show();
    });
 $('#nav-list2').live('tap', function() {
        $('#content-manual').hide();	
        $('#content-list1').hide();
	$('#content-list3').hide();
	$('#content-list4').hide();
        $('#content-list2').show();
    });
	
 $('#nav-list3').live('tap', function() {
        $('#content-manual').hide();	
        $('#content-list2').hide();
	$('#content-list1').hide();
	$('#content-list4').hide();
        $('#content-list3').show();
    });
	
 $('#nav-list4').live('tap', function() {
        $('#content-manual').hide();	
        $('#content-list2').hide();
	$('#content-list3').hide();
	$('#content-list1').hide();
        $('#content-list4').show();
    });
		

    $("#pauseaudio").live('click', function() {
        pauseAudio();
    });
    
    $("#stopaudio").live('tap', function() {
        stopAudio();
    });
    
    $('#music1').click(function() {
    	src = 'http://www.universal-soundbank.com/mp3/sounds/10157.mp3';
    	playAudio(src);
    });
    
    $('#music2').click(function() {
    	src = 'http://www.universal-soundbank.com/mp3/sounds/10183.mp3';
    	playAudio(src);
    });
    
    $('#music3').click(function() {
    	src = 'http://www.universal-soundbank.com/mp3/sounds/10153.mp3';
    	playAudio(src);
    });
    
    $('#music4').click(function() {
    	src = 'http://stream1.addictradio.net/addictrock.mp3';
    	playAudio(src);
    });
    
    $("#searchButtonConcert").click(function() {
	    // get the vars : artistName and cityName
	    artistName=$('#artistNameConcert').val();	
	    nameCity=$('#cityName').val();
	    // call the function to make an ajax request
        searchConcerts(artistName, nameCity);
    });
    
    $("#searchButtonTour").click(function() {
        nameArtist=$('#artistNameTour').val();
        nameTour=$('#tourName').val();
	    searchTour(nameArtist, nameTour);
    });

    // implement a function to read a file from the sdcard
    $(document).on('click', '.music-file', function(){
        console.log($(this).attr("uri"));
        var uri = $(this).attr("uri");
        playAudio(uri);
    });
    $(document).on('click', '.directory', function(){
        console.log($(this).attr("uri"));
        path = $(this).attr("uri");
        filesystem.root.getDirectory(path,  {create: false}, 
            function (dirEntry) {
                var patt = new RegExp("^(.*/)[^/]+/$");
                var res = patt.exec(path);
                parentDirectoryPath = res[1];
                directoryPath = path;
                parseEntries(dirEntry.createReader())
            }, function (error) {
                console.log("Unable to create new directory: " + error.code);
            }
        )
    });

    $sdgrid=$("#sd-grid");
    
});


